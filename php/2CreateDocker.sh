#!/bin/bash

docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker rmi $(docker images -q --filter "dangling=true")

cd ~/Source/shafinlearning/php/ubuntu/
docker build -t shafin/ubuntu:14.04 .

cd ~/Source/shafinlearning/php/
docker build -t shafin/shafinphp:latest .
