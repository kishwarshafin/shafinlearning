#!/bin/bash
sudo add-apt-repository ppa:chris-lea/node.js
sudo apt-get update
sudo apt-get install nodejs
sudo npm install -g npm

mkdir ~/npm
echo 'export PATH=~/npm/bin:$PATH' >> ~/.bashrc && source ~/.bashrc
echo 'export NODE_PATH=~/npm/lib/node_modules:$NODE_PATH' >> ~/.bashrc && source ~/.bashrc
npm config set prefix ~/npm

npm install -g bower
npm install -g gulp

#php
sudo apt-get update
sudo apt-get install python-software-properties
sudo add-apt-repository ppa:ondrej/php5-5.6
sudo apt-get install php5
sudo apt-get install php5-mcrypt
sudo apt-get install php5-xdebug
sudo apt-get install php5-pgsql
sudo apt-get install php5-sqlite

#composer
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer
composer selfupdate

#laravel
composer global require "laravel/installer=~1.1"
export PATH="~/.composer/vendor/bin/:$PATH"

## Apache Server
sudo php5enmod mcrypt
sudo a2enmod php5
sudo service apache2 restart

cd ~/Source
git clone git@bitbucket.org:kishwarshafin/spicialgowebsite.git
cd spicialgowebsite
composer install
npm install
bower install
gulp
php artisan serve
