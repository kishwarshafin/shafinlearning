#!/bin/bash

cd ~/Source
rm -r spicialgowebsite
git clone git@bitbucket.org:kishwarshafin/spicialgowebsite.git
cd spicialgowebsite
git fetch
git checkout master
composer install
bower install
gulp
php artisan serve
